<?php
/*
Copyright (c) 2016, Gody - ORM
All rights reserved.
*/

/******************************************
* Begin Form configuration
******************************************/

$tform_def_file = "form/proxmox_vm_informations.tform.php";

/******************************************
* End Form configuration
******************************************/

require_once '../../lib/config.inc.php';
require_once '../../lib/app.inc.php';

include './lib/pve2_api.class.php';

//* Check permissions for module
$app->auth->check_module_permissions('proxmox');

// Loading classes
$app->uses('tpl,tform,tform_actions');
$app->load('tform');

class page_action extends tform_actions {
		
	function onShowEnd() {
		global $app, $conf;
	
		$pve2 = new PVE2_API($conf["pve_link"], $conf["pve_username"], $conf["pve_realm"], $conf["pve_password"]);
		
		if ($pve2) {
			if ($pve2->login()) {
				
				$vm_id = $app->functions->intval($this->dataRecord['vm_id']);
				$vm_containers = $this->dataRecord['vm_containers'] ;
				
				$vm_temp = $pve2->get("/cluster/resources");
				$key = array_search($vm_id, array_column( $vm_temp , 'vmid'));
				$vm_pvesvr = $vm_temp[$key]['node'];
		
				$app->tpl->setVar("vm_id", $vm_id);
				$app->tpl->setVar("vm_pvesvr", $vm_pvesvr);
				
				switch($_REQUEST['next_tab'])
				{
					case 'graphics':
						//DO SOMETHING HERE
						break; 
					
					case 'informations':
					default:
						$vm_status = $pve2->get("/nodes/{$vm_pvesvr}/{$vm_containers}/{$vm_id}/status/current");
					
						if ($vm_status != false)
						{
							$app->tpl->setVar("vm_name", $vm_status['name']);
							$app->tpl->setVar("vm_status", $vm_status['status']);
							$app->tpl->setVar("vm_uptime", $app->functions->intval($vm_status['uptime'] / 60 ) );
							$app->tpl->setVar("vm_load", number_format( $vm_status['cpu'], 2 ) );
							$app->tpl->setVar("vm_cpu",  $vm_status['cpus'] );
							$app->tpl->setVar("vm_mem", $app->functions->intval($vm_status['mem']/1024/1024 ) );
							$app->tpl->setVar("vm_maxmem", $app->functions->intval($vm_status['maxmem']/1024/1024 ) );
							$app->tpl->setVar("vm_maxhdd", $app->functions->intval($vm_status['maxdisk'] /1024 /1024 / 1024 ) );
					
							$vm_percent_used = ($vm_status['mem'] * 100) / $vm_status['maxmem'] ;
							$app->tpl->setVar("used_percentage", $app->functions->intval($vm_percent_used) );
						}
						else
						{
							$app->error($app->tform->wordbook["vm_err_assignation"]);
						}
						
						$vm_config = $pve2->get("/nodes/{$vm_pvesvr}/{$vm_containers}/{$vm_id}/config");
						
						$keys = array_keys($vm_config);
						$net_temp = preg_grep('/^net[0-9]+/',$keys);
						
						$arr_net = array();
						foreach($net_temp as $net)
						{
							$settings_temp = explode(',', $vm_config[$net]);
							
							$arr_net[$net]['interface'] = $net;
							
							foreach($settings_temp as $settings )
							{
								list($k, $v) = explode('=', $settings);
								
								if (preg_match('/(e1000|vmxnet3|virtio|rtl8139)/i', $k ) )
								{
									$arr_net[$net]['mac'] = $v;
								}
								$arr_net[$net]['link_state'] = ($k == 'link_down' && $v == 1 ? 'Yes' : 'No');
								$arr_net[$net][$k] = $v;
							}
						}
						
						$app->tpl->setloop('networks', $arr_net);
						
						break;
				}
				
			} else {
				//print("Login to Proxmox Host failed.\n");
				$app->error($app->tform->wordbook["vm_err_login"]);
				exit;
			}
		} else {
			//print("Could not create PVE2_API object.\n");
			$app->error($app->tform->wordbook["vm_err_api_obj"]);
			exit;
		}
			
		parent::onShowEnd();
	}
}

$page = new page_action;
$page->onLoad();

?>
